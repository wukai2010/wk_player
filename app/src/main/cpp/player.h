//
// Created by wukai on 2021/10/21.
//

#ifndef WK_PLAYER_PLAYER_H
#define WK_PLAYER_PLAYER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <jni.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <android/log.h>
#include <time.h>
#include <utime.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/syscall.h>
#include <sched.h>
#include "libavutil/log.h"
#include "libavutil/time.h"
#include "libavutil/samplefmt.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"
#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"
#include <libavutil/imgutils.h>
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#if CONFIG_AVFILTER
#include "libavfilter/avfilter.h"
#endif

#define TAG "wk_player"
#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, TAG, __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, TAG, __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)

const int8_t WK_PLAYER_STATUS_IDLE = 0x01;
const int8_t WK_PLAYER_STATUS_READY = 0x02;
const int8_t WK_PLAYER_STATUS_PAUSE = 0x03;
const int8_t WK_PLAYER_STATUS_PLAYING = 0x04;
const int8_t WK_PLAYER_STATUS_STOP = 0x05;

extern  int player_status;

typedef struct PacketQueue {
    AVPacketList *first_pkt, *last_pkt;
    int nb_packets;
    int size;
    int abort_request;
    int serial;
    pthread_mutex_t *mutex;
};

void packet_queue_init(struct PacketQueue *queue) ;
int packet_queue_get(struct PacketQueue *queue, AVPacket *pkt);
int packet_queue_put(struct PacketQueue *queue, AVPacket *pkt) ;

typedef struct PlayContext {
    AVFormatContext *fmt_ctx;
    AVCodecContext *acodec_ctx;
    AVCodecContext *vcodec_ctx;
    AVStream *vstream;
    AVStream *astream;
    AVCodec *vcodec;
    AVCodec *acodec;
    struct PacketQueue *video_queue;
    struct PacketQueue *audio_queue;
    const char *videoFile;
};

extern struct PlayContext playContext;

void packet_queue_init(struct PacketQueue *q) ;
int packet_queue_get(struct PacketQueue *q, AVPacket *pkt);
int packet_queue_put(struct PacketQueue *q, AVPacket *pkt) ;

void* decode_video(void *argv);
void* open_video(void *args);
int initPlayer();
int32_t setBuffersGeometry(int32_t width, int32_t height);
void renderSurface(uint8_t *pixel);

JNIEXPORT jint JNICALL
Java_com_wukai_player_play_WKVideoPlayer_init(JNIEnv *env, jobject clazz,jobject surface);
JNIEXPORT jint JNICALL
Java_com_wukai_player_play_WKVideoPlayer_play(JNIEnv *env, jobject clazz, jstring path);
JNIEXPORT jint JNICALL
Java_com_wukai_player_play_WKVideoPlayer_resume(JNIEnv *env, jobject that);
JNIEXPORT jint JNICALL
Java_com_wukai_player_play_WKVideoPlayer_pause(JNIEnv *env, jobject that);
JNIEXPORT jint JNICALL
Java_com_wukai_player_play_WKVideoPlayer_release(JNIEnv *env, jobject that);




#ifdef __cplusplus
}
#endif

#endif