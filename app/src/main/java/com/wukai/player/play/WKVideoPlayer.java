package com.wukai.player.play;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import java.io.File;

public class WKVideoPlayer extends SurfaceView implements SurfaceHolder.Callback {
    private String mVideoPath;

    static {
        /**
         * avcodec-57
         *         avdevice-57
         *         avfilter-6
         *         avformat-57
         *         avutil-55
         *         postproc-54
         *         swresample-2
         *         swscale-4
         */
        System.loadLibrary("wk_player");

//        System.loadLibrary("avcodec-57");
//        System.loadLibrary("avformat-57");
//        System.loadLibrary("avdevice-57");
//        System.loadLibrary("avfilter-6");
//        System.loadLibrary("avutil-55");
//        System.loadLibrary("postproc-54");
//        System.loadLibrary("swresample-2");
//        System.loadLibrary("swscale-4");
    }

    public WKVideoPlayer(Context context) {
        super(context);
        init();
    }

    public WKVideoPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        getHolder().addCallback(this);
    }

    public void setVideoSource(File videoFile) {
        if (videoFile != null) {
            mVideoPath = videoFile.getAbsolutePath();
        }
    }

    public void start() {
        play(mVideoPath);
    }

    public void pauseVideo() {
        pause();
    }

    public void releaseVideo() {
        release();
    }

    public void resumeVideo() {
        resume();
    }

    private native int play(String path);
    private native int init(Surface surface);
    private native int pause();

    private native int release();

    private native int resume();

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        init(holder.getSurface());
    }
    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {

    }
}
