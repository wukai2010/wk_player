package com.wukai.player;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.os.Bundle;
import android.os.Environment;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Toast;

import com.tbruyelle.rxpermissions2.RxPermissions;
import com.wukai.player.databinding.ActivityMainBinding;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    String path = "/tencent/MicroMsg/WeiXin/wx_camera_1583995671979.mp4";
    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        new RxPermissions(this).requestEachCombined(
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        ).subscribe(permission -> {
            if (permission.granted) {
                Toast.makeText(getApplicationContext(),"已授权",Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding.videoView.releaseVideo();
    }
    public void onPlay (View view){
        File videoFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+path);
        binding.videoView.setVideoSource(videoFile);
        binding.videoView.start();
    }
}